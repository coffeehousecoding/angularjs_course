describe("controller test suite", function() {
	
	var $rootScope, $scope, createCtrl, CodeSamplesSvc, $httpBackend;
	
	beforeEach(module("angularJSCourseApp"));
		
	beforeEach(inject(function($injector) {
		
		$rootScope = $injector.get("$rootScope");

		CodeSamplesSvc = $injector.get("CodeSamplesSvc");

		var $controller = $injector.get("$controller");
		
		$httpBackend = $injector.get("$httpBackend");
		
		$httpBackend.when("GET", "../code-samples").respond({codeSamples : [{name:"Hello World"}]});
		
		
		$scope = $rootScope.$new();
		
		createCtrl = function(ctrlName) {
			return $controller(ctrlName, {'$scope': $scope, 'CodeSamplesSvc': CodeSamplesSvc});
		}

	}));
	
	afterEach(function() {
		
		$httpBackend.verifyNoOutstandingExpectation();
		$httpBackend.verifyNoOutstandingRequest();
		
	});
	
	it("load code samples", function() {
		
		$httpBackend.expectGET("../code-samples");
		
		var ctrl = createCtrl("HomeCtrl");
		
		$httpBackend.flush();
		
		expect($scope.codeSamples.length).toBe(1);
	});

})