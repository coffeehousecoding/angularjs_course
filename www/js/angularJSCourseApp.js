"use strict";

var angularJSCourseApp = (function($, angular, document) {
	
	angular.module("AngularJSCourseApp", ['ngSanitize'])
		.factory("CodeSamplesSvc", ["$http", function($http) {
			return { 
				
				getAll: function() { return $http.get("../code-samples"); },
				
				getCodeSampleSourceCode: function(url) { return $http.get(url); }
				
			};
		}])
		.factory("ConsoleSvc", ["$log", function($log) {
			
			var counter = 0;
			
			return { log: function(msg) { counter++; return $log.info(counter + ": " + msg); } };
		}])
		.directive("ngcourseModal", function(ConsoleSvc) {
			return {
				restrict: "E",
				transclude: true,
				templateUrl: "templates/modal_window.html",
				controller: function($scope, $element, $attrs) {
					$scope.modalId = $attrs["modalId"];
				} 
			}
		})
		.directive("ngcourseTabs", function(ConsoleSvc) {
			
			return {
				
				restrict: "E",
				transclude:true,
				scope: true,
				templateUrl: "templates/tabs.html",
				controller: function($scope, $element, $attrs, $transclude) {
					$scope.tabs = [];
					this.addTab = function(tabId, tabTitle, tabActive) {
						$scope.tabs.push({ tabId: tabId, tabTitle: tabTitle, tabActive: tabActive });
					}
					
					this.activateTab = function(tabId) {
						
						for (var tabIndex=0; tabIndex<$scope.tabs.length; tabIndex++) {
						
							if ($scope.tabs[tabIndex].tabId === tabId) {
								$("#tab-" + $scope.tabs[tabIndex].tabId).addClass("active");
							} else {
								$("#tab-" + $scope.tabs[tabIndex].tabId).removeClass("active");
							}
							
						}
						
					};
				}
				
			}
			
		})
		.directive("ngcourseTab", function(ConsoleSvc) {
			
			return {
				restrict: "E",
				transclude:true,
				replace: true,
				templateUrl: "templates/tab.html",
				require: "^ngcourseTabs",
				scope: true,
				link: function(scope, element, attrs, ctrl) {
					
					ctrl.addTab(scope.tabId, scope.tabTitle, scope.tabActive);

					if (scope.tabActive) {
						element.addClass("active");
					}
					
					scope.$on("activate_tab", function(event, options) {
						
						if (scope.tabId == options.tabId) {
							scope.tabActive = true;
							element.addClass("active");
							ctrl.activateTab(options.tabId);
							
						} else {
							scope.tabActive = false;
							element.removeClass("active");
						}
						
					});
				},
				controller: function($scope, $element, $attrs, $transclude) {
					
					$scope.tabId = $attrs["tabId"];
					$scope.tabTitle = $attrs["tabTitle"];
					$scope.tabActive = $attrs["tabActive"] === "true";

					$transclude($scope.$new(), function(clone) {
						$element.append(clone);
					});
					

				}
				
			}
			
		})
		.directive("ngcourseSourceCode", function($sanitize) {
			
			return {
				restrict: "E",
				scope: {
					sourceCode: "="
				},
				templateUrl: "templates/source_code.html",
				link: function(scope, element, attrs) {
					
					scope.$watch("sourceCode", function() {
						if (!angular.isUndefined(scope.sourceCode) && scope.sourceCode !== null) {
							
							var sourceCodeLines = scope.sourceCode.split(/\r\n|\r|\n/g);
							var div = document.createElement('div');
							
							for(var x=0; x<sourceCodeLines.length; x++) {
								div.innerText = sourceCodeLines[x];
								sourceCodeLines[x] = div.innerHTML.replace(/\t/g, "&nbsp;&nbsp;").replace(/&lt;/g, "<span>&lt;").replace(/&gt;/g, "&gt;</span>");
							}
							
							scope.sourceCodeLines = sourceCodeLines;
							
						}
					});

				}
			}
			
		})
		.controller("HomeCtrl", ["$scope", "$rootScope", "CodeSamplesSvc", function($scope, $rootScope, CodeSamplesSvc) {
		
			$scope.level = "";
			$scope.codeSampleTabs = [];
		
			$scope.runCodeSample = function($event, codeSample) {
				$rootScope.$broadcast("showCodeSampleModal", codeSample);
			}
	    
			$scope.codeSamples = CodeSamplesSvc.getAll().success(function (data, status, headers) {
				$scope.codeSamples = data.codeSamples;    
			});
	    
		}])
		.controller("CodeSampleModalCtrl", ["$scope", "CodeSamplesSvc", function($scope, CodeSamplesSvc) {
		
			$scope.$on("showCodeSampleModal", function(event, options) {
				$scope.codeSampleName = options.name;
				$scope.url = options.url;
				CodeSamplesSvc.getCodeSampleSourceCode(options.url).success(function(data) {
					$scope.sourceCode = data;	
				});
				
				$scope.$broadcast("activate_tab", { tabId: "live" })

				$("#codeSampleModal").modal();
			});

		}]);
	
	return angular.module("AngularJSCourseApp");

}(jQuery, angular, document));