module.exports = function(app, db, mongo){

	return {
		
		configureRoutes: function() {
			
			app.use("/code-samples", function(req, res) {
				db.collection("codeSamples").find().toArray(function(err, itemArr) {
					res.json({ codeSamples: itemArr });
				})
			});
		}
	}

};