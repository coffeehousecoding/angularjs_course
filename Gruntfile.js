"use strict";

module.exports = function(grunt) {

	grunt.initConfig({
		bower: {
			install: {
				options: {
					targetDir: './www/lib',
					layout: 'byType',
					install: true,
					verbose: false,
					cleanTargetDir: false,
					cleanBowerDir: false,
					bowerOptions: {}
				}
			}
		},		
		connect: { 
			path: "./www",
			port: process.env.PORT
		},
		express: {
			appPath: "./www",    
			samplesPath: "./samples",    
			port: process.env.PORT
		},
		mongo: {
			user: "ngcourserouser",
			pass: "password!",
			host: "ds037447.mongolab.com",
			port: 37447,
			db: "angularjs_course"
		},
		less: {
			development: {
				options: {
					compress: true,
					yuicompress: true,
					optimization: 2
				},
				files: {
					// target.css file: source.less file
					"www/css/site.css": "less/site.less"
				}
			}
		},
		watch: {
			styles: {
				files: ['less/**/*.less'], // which files to watch
				tasks: ['less'],
				options: {
					nospawn: true
				}
			}
		}
	});
    
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-bower-task');

	grunt.registerTask("watch-less", ['watch']);

	grunt.registerTask("connect-server", "start connect web server", function() {
        
		this.async();

		var options = grunt.config.get("connect");
		var connect = require('connect');
		var serveStatic = require('serve-static');

		var app = connect();
		app.use(serveStatic(options.path));
		app.listen(options.port);

		grunt.log.writeln("server started on port " + options.port);
	});
    
	grunt.registerTask("express-server", "start an express web server", function() {
       
    this.async();
      
    var expressOptions = grunt.config.get("express");
		var mongoOptions = grunt.config.get("mongo");
     
		var express = require("express");
		var http = require("http");
		var bodyParser = require("body-parser");
		var codeSamples = require("./rest/code-samples.js");
		var mongo = require("mongodb");
       
		mongo.connect("mongodb://" + mongoOptions.user + ":" + mongoOptions.pass + "@" + mongoOptions.host + ":" + mongoOptions.port + "/" +mongoOptions.db, function(err, db) {
			
			if (err) {
			
				grunt.log.writeln("Failed to connect to the MongoDB server.");
				grunt.log.writeln(err);
				return;

			}
			
			grunt.log.writeln("Successfully connected to MongoDB '" + mongoOptions.db + "' at '" + mongoOptions.host + ":" + mongoOptions.port + "'.");

			var app = express();
			app.use("/", express.static(expressOptions.appPath));
			app.use("/samples", express.static(expressOptions.samplesPath));
			app.use(bodyParser.json());
			codeSamples(app, db, mongo).configureRoutes();
			
			http.createServer(app).listen(expressOptions.port);
			grunt.log.write("Successfully started an Express web server on port " + expressOptions.port + ".");

		});

	});

}